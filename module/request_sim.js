function dump(obj)
	{
		var out = '';
		for (var i in obj) {out += i + "= " + obj[i] + "\n";}
		console.log(out);
	};

function Json_to_tab(data_json)
	{
		var tab = new Object();	
		data_json = data_json.replace("{","");
		data_json = data_json.replace("}", "");
		data_json=data_json.replace(/"/g, "");
		data_json=data_json.replace(/ /g, "_");
		data_json=data_json.replace(/\n/g, "");
		data_json=data_json.replace(/\r/g, "");
		data_json = data_json.split(",");

		for (var i = 0, len = data_json.length; i < len; i++)
			{
				var temp=data_json[i].split(":");
				var key=temp[0];
				var value=temp[1];
				value=value.replace(".",",");
				tab[key.trim()]=value.trim();
			}
		return tab;
	};
function wait(ms)
			{
				var d = new Date();
				var d2 = null;

				do { d2 = new Date(); } 
				while(d2-d < ms);
			}

function sim_req_bind (url,parametre,commande,ok,pas_ok)
	{
		if (parametre=='none')
			{
				var options ={
									'uri': url,
									'method': 'POST',
									'Headers' : 'Content-type: application/json',
									'json': {"cmd": commande},
								};
			}
		else
			{
				var options = 	{
							'uri': url,
							'method': 'POST',
							'Headers' : 'Content-type: application/json',
							'json': {"cmd": commande , "param": parametre},
						};
			}
		
		request=require('request');
		request(options, function (err, response, body)
			{
				if (!err && response.statusCode == 200)
					{
						SARAH.speak(ok);
					}	
				else
					{
						SARAH.speak(pas_ok);
					}
			});
							
	};

function sim_req_full (url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
	{
		var options2 = {'uri': url2,'method': 'GET',};
		
		if (parametre=='none')
			{
				var options ={
									'uri': url,
									'method': 'POST',
									'Headers' : 'Content-type: application/json',
									'json': {"cmd": commande},
								};
			}
		else
			{
				var options = 	{
							'uri': url,
							'method': 'POST',
							'Headers' : 'Content-type: application/json',
							'json': {"cmd": commande , "param": parametre},
						};
			}
		
		request1=require('request');
		request1(options2, function (err, response, body)
			{
				
				if (body!=undefined)
					{
						var data_req1 = Json_to_tab(body);
						if (data_req1[callback_commande]==etat) {SARAH.speak(encore);}
						else
							{
								request2=require('request');
								request2(options, function (err, response, body)
									{
										if (!err && response.statusCode == 200)
											{
												wait(2000);
												request3=require('request');
												request3(options2, function (err, response, body)
													{
														var data_req2 = Json_to_tab(body);
														if (body!=undefined)
															{
																console.log(data_req2[callback_commande]+"="+etat);
																if (data_req2[callback_commande]!=etat) {SARAH.speak(echec);}
																else {SARAH.speak(ok);}
															}
														else
															{
																SARAH.speak(pas_ok);
															}
													});
											}
										else
											{
												SARAH.speak(pas_ok);
											}
									});
							}
					}
				else
					{
						SARAH.speak(pas_ok);
					}
			});
		};

var sim_state = function (url,methode_to_c)
	{
		var options = {'uri': url,'method': 'GET',};
		
		request=require('request');
		request(options, function (err, response, body)
			{
				if (body!=undefined)
					{
						var tab = new Object();	

						tab=Json_to_tab(body.toString());

						if (methode_to_c=="none")
							{
								dump(tab);

								SARAH.speak('informations affichées !');
							}
						else if(methode_to_c=="lost") 
							{	
								SARAH.speak("Altitude:"+parseInt(tab['Plane_Altitude'])+" pieds, cap:"+parseInt(tab['Plane_Heading_Degrees_True'])+", vitesse:"+parseInt(tab['Airspeed_Indicated'])+" noeuds");
								
								var headers =	{
													'User-Agent':'Super Agent/0.0.1',
													'Content-Type':'application/x-www-form-urlencoded'
												};

								var options = 	{
													'uri': 'http://fsfrancesimulateur2.fr/v6/membre/page/tools/jarvis_lost.php',
													'method': 'POST',
													headers: headers,
    												form: {'msg': 'bonjour sarah!','lat':tab['Plane_Latitude'].replace(",","."),'lng':tab['Plane_Longitude'].replace(",",".")}
												};

								request1=require('request');
								request1(options, function (err, response, body)
									{
										if (!err && response.statusCode == 200)
											{
												var tab2 = new Object();	

												tab2=Json_to_tab(body.toString());
												//dump(tab2);
												setTimeout(
													function()
													{
														SARAH.speak("Nom de l'aéroport: "+tab2['Nom']);
														setTimeout(
															function()
																{
																	SARAH.speak("Indicatif: "+tab2['Appellation'])
																},3000);
													},3000);
												data_req_state=tab2;
											}	
										else
											{
												SARAH.speak('page non trouvée');
											}
									});

								
							}
					}
				else{SARAH.speak("Monsieur, je n'ai pas accés au données de l'appareil !");}
				
				
			});

	};

var apu_connection = function (url,url2,parametre,etat)
	{
		var commande="APU_GENERATOR_SWITCH_SET";
		var pas_ok="pas de réponse du circuit générateur auxiliaire";
				
		
		if (parametre==1)
			{
				var ok="Connexion du générateur auxiliaire";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
		else if(parametre==0)
			{
				var ok="Déconnexion du générateur auxiliaire";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
		
	}
exports.apu_connection = apu_connection;

var apu_engine = function (url,url2,parametre,etat)
	{
		if (parametre==1)
			{
				var commande="APU_STARTER";
				var ok="démarrage du générateur auxiliaire";
				var pas_ok="pas de réponse du générateur auxiliaire";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
		else if(parametre==0)
			{
				var commande="APU_OFF_SWITCH";
				var ok="coupure du générateur auxiliaire";
				var pas_ok="pas de réponse du générateur auxiliaire";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
		
	}
exports.apu_engine = apu_engine;

var switch_batterie = function (url,url2,parametre,etat)
	{
		var commande="TOGGLE_MASTER_BATTERY";
		var ok="Batterie commuté";
		var pas_ok="pas de réponse du circuit principal";
		sim_req_bind (url,parametre,commande,ok,pas_ok)
	}
exports.switch_batterie = switch_batterie;

var switch_gen = function (url,url2,parametre,etat)
	{
		var commande="TOGGLE_MASTER_ALTERNATOR";
		var ok="Générateur commuté";
		var pas_ok="pas de réponse des générateur";
		sim_req_bind (url,parametre,commande,ok,pas_ok)
	}
exports.switch_gen = switch_gen;

var autopilot = function (url,url2,parametre,etat)
	{
		var callback_commande="Autopilot_Master";
		var pas_ok="Pas de réponse de l'auto-pilote monsieur";
				

		if (etat==1)
			{
				var commande="AUTOPILOT_ON";
				var ok="Auto-pilote activée";
				var encore="l'autopilote est déja activé";
				var echec="monsieur l'autopilote ne veux pas s'activer";
				sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
			}
		else if(etat==0)
			{
				var commande="AUTOPILOT_OFF";
				var ok="Auto-pilote désactivée";
				var encore="l'autopilote est déja désactivé";
				var echec="monsieur l'autopilote ne veux pas se désactiver";
				sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
			}
	}
exports.autopilot = autopilot;

var select_hdg = function (url,url2,parametre,etat)
	{
		var commande="HEADING_BUG_SET";
		var callback_commande="Autopilot_Heading_Lock_Dir";
		var ok="Cap modifié au " +parametre;
		var pas_ok="pas de réponse du conservateur de cap monsieur";
		var encore="le cap est déjà réglé sur "+parametre+ "";
		var echec="monsieur pas moyen de régler le conservateur de cap";
		sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
	}
exports.select_hdg = select_hdg;

var auto_hdg = function (url,url2,parametre,etat)
	{
		var callback_commande="Autopilot_Heading_Lock";
		var pas_ok="pas de réponse du conservateur de cap";
				

		if (etat==1)
			{
				var commande="AP_HDG_HOLD_ON";
				var ok="Conservateur de cap activée";
				var encore="le conservateur de cap est déjà activée";
				var echec="monsieur pas moyen d'activer le conservateur de cap";
				sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
			}
		else if(etat==0)
			{
				var commande="AP_HDG_HOLD_OFF";
				var callback_commande="Autopilot_Heading_Lock";
				var ok="Conservateur de cap désactivée";
				var encore="le conservateur de cap est déjà désactivée";
				var echec="monsieur pas moyen de désactiver le conservateur de cap";
				sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
			}
	}
exports.auto_hdg = auto_hdg;

var select_alt = function (url,url2,parametre,etat)
	{
		var commande="AP_ALT_VAR_SET_ENGLISH";
		var callback_commande="Autopilot_Altitude_Lock_Var";
		var ok="altitude modifié à " +parametre+ " pieds ()";
		var pas_ok="pas de réponse du conservateur d'altitude monsieur";
		var encore="l'altitude est déjà réglé sur "+parametre+ " pieds ()";
		var echec="monsieur pas moyen de régler le conservateur d'altitude";
		sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
	}
exports.select_alt = select_alt;

var auto_alt = function (url,url2,parametre,etat)
	{
		var callback_commande="Autopilot_Altitude_Lock";
		var pas_ok="pas de réponse du conservateur d'altitude";
				

		if (etat==1)
			{
				var commande="AP_ALT_HOLD_ON";
				var ok="Conservateur d'altitude activée";
				var encore="le conservateur d'altitude est déjà activée";
				var echec="monsieur pas moyen d'activer le conservateur d'altitude";
				sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
			}
		else if(etat==0)
			{
				var commande="AP_ALT_HOLD_OFF";
				var ok="Conservateur d'altitude désactivée";
				var encore="le conservateur d'altitude est déjà désactivée";
				var echec="monsieur pas moyen de désactiver le conservateur d'altitude";
				sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
			}
	}
exports.auto_alt = auto_alt;

var speed_alt = function (url,url2,parametre,etat)
	{
		var commande="AP_SPD_VAR_SET";
		var callback_commande="Autopilot_Airspeed_Hold_Var";
		var ok="Vitesse modifiée à " +parametre + "noeuds";
		var pas_ok="pas de réponse du conservateur de vitesse";
		var encore="la vitesse est déjà réglée sur "+parametre+ "noeuds";
		var echec="monsieur pas moyen de régler le conservateur de vitesse";
		sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
	}
exports.speed_alt = speed_alt;

var auto_speed = function (url,url2,parametre,etat)
	{
		var callback_commande="Autopilot_Airspeed_Hold";
		var pas_ok="pas de réponse du conservateur de vitesse";
				

		if (etat==1)
			{
				var commande="AP_AIRSPEED_ON";
				var ok="Conservateur de vitesse activée";
				var encore="Conservateur de vitesse est déjà activée";
				var echec="monsieur pas moyen d'activer le conservateur de vitesse";
				sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
			}
		else if(etat==0)
			{
				var commande="AP_AIRSPEED_OFF";
				var ok="Conservateur de vitesse désactivée";
				var encore="Conservateur de vitesse est déjà désactivée";
				var echec="monsieur pas moyen de désactiver le conservateur de vitesse";
				sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
			}
	}
exports.auto_speed = auto_speed;

var auto_throttle = function (url,url2,parametre,etat)
	{
		var callback_commande="Autopilot_Throttle_Arm";
		var commande="AUTO_THROTTLE_ARM";
		var pas_ok="pas de réponse de la manette des gaz";

		if (etat==1)
			{
				var ok="Manette des gaz controlé";
				var encore="j'ai déja le controle de la manette des gaz";
				var echec="monsieur pas moyen de prendre le controle de la manette des gaz";
				sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
			}
		else if(etat==0)
			{
				var ok="Manette des gaz débrayée";
				var encore="vous avez déjà la manette des gaz sous votre controle";
				var echec="monsieur pas moyen de débrayer la manette des gaz";
				sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
			}
	}
exports.auto_throttle = auto_throttle;

var auto_appr = function (url,url2,parametre,etat)
	{
		var callback_commande="Autopilot_Approach_Hold";
		var pas_ok="pas de réponse du circuit d'approche";

		if (etat==1)
			{
				var commande="AP_APR_HOLD_ON";
				var ok="Approche activée";
				var encore="l'approche est déjà activée";
				var echec="monsieur pas moyen d'activer l'approche";
				sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
			}
		else if(etat==0)
			{
				var commande="AP_APR_HOLD_OFF";
				var ok="Approche désactivée";
				var encore="l'approche est déjà désactivée";
				var echec="monsieur pas moyen de désactiver l'approche";
				sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
			}
	}
exports.auto_appr = auto_appr;

var auto_nav = function (url,url2,parametre,etat)
	{
		var callback_commande="Autopilot_Nav1_Lock";
		var pas_ok="pas de réponse du circuit de navigation";
				

		if (etat==1)
			{
				var commande="AP_NAV1_HOLD_ON";
				var ok="Navigation activée";
				var encore="la navigation est déjà activée";
				var echec="monsieur pas moyen d'activer la navigation";
				sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
			}
		else if(etat==0)
			{
				var commande="AP_NAV1_HOLD_OFF";
				var ok="Navigation désactivée";
				var encore="la navigation est déjà désactivée";
				var echec="monsieur pas moyen de désactiver la navigation";
				sim_req_full(url,url2,parametre,commande,callback_commande,ok,pas_ok,encore,echec,etat)
			}
	}
exports.auto_nav = auto_nav;

var select_crs = function (url,url2,parametre,etat)
	{
		var commande="VOR1_SET";
		var ok="Course modifié au " +parametre;
		var pas_ok="pas de réponse du conservateur de course monsieur";
		sim_req_bind (url,parametre,commande,ok,pas_ok)
	}
exports.select_crs = select_crs;

var nav_switch = function (url,url2,parametre,etat)
	{
		var commande="NAV1_RADIO_SWAP";
		var ok="navigation commuté";
		var pas_ok="Pas de réponse de la radio nav";
		sim_req_bind (url,parametre,commande,ok,pas_ok)
	}
exports.nav_switch = nav_switch;

var com_switch = function (url,url2,parametre,etat)
	{
		var commande="COM_STBY_RADIO_SWAP";
		var ok="communication commuté";
		var pas_ok="Pas de réponse de la radio com";
		sim_req_bind (url,parametre,commande,ok,pas_ok)
	}
exports.com_switch = com_switch;

var select_crs = function (url,url2,parametre,etat)
	{
		var commande="VOR1_SET";
		var ok="Course modifié au " +parametre;
		var pas_ok="pas de réponse du conservateur de course monsieur";
		sim_req_bind (url,parametre,commande,ok,pas_ok)
	}
exports.select_crs = select_crs;

var select_nav = function (url,url2,parametre,etat)
	{
		var commande="NAV1_STBY_SET";
		var ok="Radio navigation réglé sur " +etat;
		var pas_ok="pas de réponse de la radio nav";
		sim_req_bind (url,parametre,commande,ok,pas_ok)
	}
exports.select_nav = select_nav;

var select_com = function (url,url2,parametre,etat)
	{
		var commande="COM_STBY_RADIO_SET";
		var ok="Radio communication réglé sur " +etat;
		var pas_ok="pas de réponse de la radio com";
		sim_req_bind (url,parametre,commande,ok,pas_ok)
	}
exports.select_com = select_com;



var gear = function (url,url2,parametre,etat)
	{
		var pas_ok="pas de réponse du train d'aterrissage";

		if (parametre==1)
			{
				var commande="GEAR_DOWN";
				var ok="Sortie du train";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
		else if(parametre==0)
			{
				var commande="GEAR_UP";
				var ok="Rentrée du train";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
	}
exports.gear = gear;

var spoiler = function (url,url2,parametre,etat)
	{
		var pas_ok="pas de réponse des aérofreins";

		if (parametre==1)
			{
				var commande="SPOILERS_ON";
				var ok="Ouverture des aérofreins";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
		else if(parametre==0)
			{
				var commande="SPOILERS_OFF";
				var ok="Fermeture des aérofreins";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
	}
exports.spoiler = spoiler;

var engine_auto_ign = function (url,url2,parametre,etat)
	{
		var pas_ok="pas de réponse de la gestion moteur";

		if (parametre==1)
			{
				var commande="ENGINE_AUTO_START";
				var ok="Lancement de la séquence de démarrage moteur";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
		else if(parametre==0)
			{
				var commande="ENGINE_AUTO_SHUTDOWN";
				var ok="Coupure des moteurs";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
	}
exports.engine_auto_ign = engine_auto_ign;

var engine_set = function (url,url2,parametre,etat)
	{
		var commande="THROTTLE_SET";
		var pas_ok="pas de réponse des moteurs";

		if (parametre==-16383)
			{
				
				var ok="Inversion de la poussée";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
		else if(parametre==0)
			{
				var ok="Moteurs au ralentit";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
		else if(parametre==16383)
			{
				var ok="Moteur à pleine puissance";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
		else
			{
				var ok="Puissance moteur à "+etat+" pour cent";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
	}
exports.engine_set = engine_set;

var flaps = function (url,url2,parametre,etat)
	{
		var pas_ok="pas de réponse des volets";

		if (parametre==100)
			{
				var commande="FLAPS_DOWN";
				var parametre="none";
				var ok="Sortie des volets à pleine ouverture";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
		else if(parametre==0)
			{
				var commande="FLAPS_UP";
				var parametre="none";
				var ok="Rentrée des volets";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
		else if(parametre=="FLAPS_DECR")
			{
				var commande="FLAPS_DECR";
				var parametre="none";
				var ok="Rentrée des volets";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
		else if(parametre=="FLAPS_INCR")
			{
				var commande="FLAPS_INCR";
				var parametre="none";
				var ok="Sortie des volets";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
		else
			{
				var commande="FLAPS_SET";
				var ok="Positionnements des volets à "+etat+" pour cent";
				sim_req_bind (url,parametre,commande,ok,pas_ok)
			}
		
	}
exports.flaps = flaps;


exports.sim_state = sim_state;
exports.sim_req_full = sim_req_full;
exports.sim_req_bind = sim_req_bind;
