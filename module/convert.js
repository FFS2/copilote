var JSON_to_array = function (JSON)
	{
		var data_json=JSON;
		data_json = data_json.replace("{","");
		data_json = data_json.replace("}", "");
		data_json=data_json.replace(/"/g, "");
		data_json=data_json.replace(/\n/g, "");
		data_json=data_json.replace(/\r/g, "");
		data_json=data_json.replace(/"/g, "");
		data_json=data_json.replace(/ /g, "_");
				
		data_json = data_json.split(",");
		var tab = new Object();					
		for (var i = 0, len = data_json.length; i < len; i++)
			{
			  	var temp=data_json[i].split(":");
				var key=temp[0];
				var value=temp[1];
				value=value.replace(".",",");
				tab[key.trim()]=value.trim();
			}
		return tab;

	}

function decbin(dec,length)
	{
		var out = "";
		while(length--)
			out += (dec >> length ) & 1;    
			return out;  
	}

var to_bcd_radio = function (integer)
	{
		function decbin(dec,length)
			{
				var out = "";
				while(length--)
					out += (dec >> length ) & 1;    
					return out;  
			}

		var string=integer.toString();
		var string_length=string.length;
		var caract;
		var text="";
		var bcd="";
		var	length=4;
		var out="";

		for (var i = 0; i < string_length; i++) {bcd += decbin(string.charAt(i),4);};

		return bcd = parseInt(bcd, 2);
	}
exports.JSON_to_array = JSON_to_array;
exports.to_bcd_radio = to_bcd_radio;