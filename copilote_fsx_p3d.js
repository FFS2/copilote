var exec = require('child_process').exec;

var url="http://127.0.0.1:8000/cmd";
var url2="http://127.0.0.1:8000/getall";
var parametre='none';
global.data_req_state=new Object();

function dump(obj)
	{
		var out = '';
		for (var i in obj) {out += i + "= " + obj[i] + "\n";}
		console.log(out);
	}

exports.action = function(data, next)
	{
		
		var d;
		var d2 = null;
		// Called by SARAH to perform main action
		info('Plugin copilote_fsx_p3d is called ...', data);

		if (!data.prg) {callback({'tts':"Désolé je n'ai pas compris"});}
		else
			{
				sim_resquest = require('./module/request_sim');
				//setTimeout(function(){SARAH.play("qrcode.mp3");},100);
				//SARAH.play("qrcode.mp3");
				//SARAH.stop("qrcode.mp3");
				//SARAH.play("/plugins/copilote_fsx_p3d/media/Speech_sarah.wav");
				
				switch(data.prg)
					{
						case "la":
							var Txt = new Array; 
							Txt[0] = "oui monsieur";
							Txt[1] = "oui monsieur, que puije pour vous";
							Txt[2] = "biensur";
							Txt[3] = "comme toujours";
							Txt[4] = "oui";
							Txt[5] = "Pour vous monsieur, toujours";
						break;

						case "bonjour":
							var Txt = new Array; 
							Txt[0] = "bonjour monsieur";
							Txt[1] = "bonjour monsieur, que puije pour vous";
							Txt[2] = "bonjour";
						break;

						case "go_fsx":
							var Txt = new Array; 
							Txt[0] = "C'est partis pour un vol, serons nous seul?";
							Txt[1] = "Dois-je prévenir les secours avant de partir?";
							Txt[2] = "Quelle est notre destination?";
							Txt[3] = "Quel appareil allons nous sortir?";
							var process ='%CD%/plugins/copilote_fsx_p3d/bin/go_fsx.bat';
						break;

						case "go_p3d":
							var Txt = new Array; 
							Txt[0] = "Vous aussi vous le trouvez plus beau?";
							Txt[1] = "Enfin sur un simulateur qui tient le pavé!";
							Txt[2] = "Votre FSX vous fait encore des misére?";
							var process ='%CD%/plugins/copilote_fsx_p3d/bin/go_p3d.bat';
						break;

						case "go_fsxml":
							var Txt = new Array; 
							Txt[0] = "Activation de la passerel";
							Txt[1] = "Interconnexion en cours de création";
							Txt[2] = "Lancement de la passerel";
							var process ='%CD%/plugins/copilote_fsx_p3d/bin/fsxml.exe';
						break;

						case "go_ffstacker":
							var Txt = new Array; 
							Txt[0] = "On leur montre qui sont les chefs?";
							Txt[1] = "Pourquoi je sens que l'on va encore être ridicule?";
							Txt[2] = "Cela faisait longetmps";
							var process ='%CD%/plugins/copilote_fsx_p3d/bin/go_ffstacker.bat';
						break;

						case "test_system":
							var methode_to_c='none';
							sim_resquest.sim_state(url2,methode_to_c);
						break;

						case "auto_on":
							var etat=1;
							sim_resquest.autopilot(url,url2,parametre,etat);
						break;

						case "auto_off":
							var etat=0;
							sim_resquest.autopilot(url,url2,parametre,etat);
						break;

						case "alt_select":
							var parametre=data.parameters;
							var etat=data.parameters;
							sim_resquest.select_alt(url,url2,parametre,etat);
						break;

						case "alt_on":
							var etat=1;
							sim_resquest.auto_alt(url,url2,parametre,etat);
						break;

						case "alt_off":
							var etat=0;
							sim_resquest.auto_alt(url,url2,parametre,etat);
						break;

						case "hdg_select":
							var parametre=data.parameters;
							var etat=data.parameters;
							sim_resquest.select_hdg(url,url2,parametre,etat);
						break;

						case "hdg_on":
							var etat=1;
							sim_resquest.auto_hdg(url,url2,parametre,etat);
						break;

						case "hdg_off":
							var etat=0;
							sim_resquest.auto_hdg(url,url2,parametre,etat);
						break;

						case "speed_select":
							var parametre=data.parameters;
							var etat=data.parameters;
							sim_resquest.speed_alt(url,url2,parametre,etat);
						break;

						case "throttle_on":
							var etat=1;
							sim_resquest.auto_throttle(url,url2,parametre,etat);
						break;

						case "throttle_off":
							var etat=0;
							sim_resquest.auto_throttle(url,url2,parametre,etat);
						break;

						case "speed_on":
							var etat=1;
							sim_resquest.auto_speed(url,url2,parametre,etat);
						break;

						case "speed_off":
							var etat=0;
							sim_resquest.auto_speed(url,url2,parametre,etat);
						break;

						case "nav1_select":
							var integer=data.parameters;
							integer=integer.replace(",", ".");
							integer=parseInt(integer*100);
							
							if (integer>10800 && integer<11795)
								{
									var convert_bcd=require('./module/convert')
									var bcd = convert_bcd.to_bcd_radio(integer);
									var parametre=bcd;
									var etat=data.parameters;
									sim_resquest.select_nav(url,url2,parametre,etat);
								}
							else
								{
									SARAH.speak("Je n'ai pas compris la fréquence");
								}
						break;

						case "switch_nav":
							sim_resquest.nav_switch(url,url2,parametre,etat);
						break;

						case "com1_select":
							var integer=data.parameters;
							integer=integer.replace(",", ".");
							integer=parseInt(integer*100);
							
							if (integer>11800 && integer<13697)
								{
							
									var convert_bcd=require('./module/convert')
									var bcd = convert_bcd.to_bcd_radio(integer);
									var parametre=bcd;
									var etat=data.parameters;
									sim_resquest.select_com(url,url2,parametre,etat);
								}
							else
								{
									SARAH.speak("Je n'ai pas compris la fréquence");
								}
						break;

						case "switch_com":
							sim_resquest.com_switch(url,url2,parametre,etat);
						break;

						case "app_on":
							var etat=1;
							sim_resquest.auto_appr(url,url2,parametre,etat);
						break;

						case "app_off":
							var etat=0;
							sim_resquest.auto_appr(url,url2,parametre,etat);
						break;

						case "nav_on":
							var etat=1;
							sim_resquest.auto_nav(url,url2,parametre,etat);
						break;

						case "nav_off":
							var etat=0;
							sim_resquest.auto_nav(url,url2,parametre,etat);
						break;

						case "crs_select":
							var parametre=data.parameters;
							sim_resquest.select_crs(url,url2,parametre,etat);
						break;

						case "gear_up":
							var parametre=0;
							sim_resquest.gear(url,url2,parametre,etat);
						break;

						case "gear_down":
							var parametre=1;
							sim_resquest.gear(url,url2,parametre,etat);
						break;

						case "spoil_on":
							var parametre=1;
							sim_resquest.spoiler(url,url2,parametre,etat);
						break;

						case "spoil_off":
							var parametre=0;
							sim_resquest.spoiler(url,url2,parametre,etat);
						break;

						case "flaps_up":
							var parametre=0;
							sim_resquest.flaps(url,url2,parametre,etat);
						break;
						
						case "flaps_down":
							var parametre=0;
							sim_resquest.flaps(url,url2,parametre,etat);
						break;

						case "flaps_incr":
							var parametre="FLAPS_INCR";
							sim_resquest.flaps(url,url2,parametre,etat);
						break;

						case "flaps_decr":
							var parametre="FLAPS_DECR";
							sim_resquest.flaps(url,url2,parametre,etat);
						break;

						case "flaps_pos":
							var etat=data.parameters;
							var integer=data.parameters;
							var commande="FLAPS_SET";
							var parametre=parseInt(16383/100*integer);
							sim_resquest.flaps(url,url2,parametre,etat);
						break;

						case "auto_start":
							var parametre=1;
							sim_resquest.engine_auto_ign(url,url2,parametre,etat);
						break;

						case "auto_shutdown":
							var parametre=0;
							sim_resquest.engine_auto_ign(url,url2,parametre,etat);
						break;

						case "engine_power":
							var integer=data.parameters;
							var parametre=parseInt(16383/100*integer);
							var etat=integer;
							sim_resquest.engine_set(url,url2,parametre,etat);
						break;

						case "full_power":
							var parametre=16383;
							sim_resquest.engine_set(url,url2,parametre,etat);
						break;

						case "off_power":
							var parametre=0;
							sim_resquest.engine_set(url,url2,parametre,etat);
						break;

						case "reverse_power":
							var parametre=-16383;
							sim_resquest.engine_set(url,url2,parametre,etat);
						break;

						case "start":
							var parametre=1;
							/**/
							setTimeout(
								function()
									{
										SARAH.speak('simulation niveaux 1');
										sim_resquest.switch_batterie(url,url2,parametre,etat);
										/**/
										setTimeout(
											function()
												{
													SARAH.speak('simulation niveaux 2');
													sim_resquest.apu_engine(url,url2,parametre,etat);
													/**/
													setTimeout(
														function()
															{
																SARAH.speak('simulation niveaux 3');
																sim_resquest.apu_connection(url,url2,parametre,etat);
																/**/
																setTimeout(
																	function()
																		{
																			SARAH.speak('simulation niveaux 4');
																			sim_resquest.engine_auto_ign(url,url2,parametre,etat);
																		}
																	,5000);
																/**/
															}
														,5000);
													/**/
												}
											,5000);
										/**/
									}
								,5000);
							/**/
						break;


						case "apu_start":
							var parametre=1;
							sim_resquest.apu_engine(url,url2,parametre,etat);
						break;

						case "apu_shutdown":
							var parametre=0;
							sim_resquest.apu_engine(url,url2,parametre,etat);
						break;

						case "apu_connect":
							var parametre=1;
							sim_resquest.apu_connection(url,url2,parametre,etat);
						break;

						case "apu_disconnect":
							var parametre=0;
							sim_resquest.apu_connection(url,url2,parametre,etat);
						break;
						
						case "batterie_switch":
							sim_resquest.switch_batterie(url,url2,parametre,etat);
						break;
						
						case "gen_switch":
							sim_resquest.switch_gen(url,url2,parametre,etat);
						break;

						case "lost":
							var Txt = new Array; 
							Txt[0] = "Localisation en cours";
							Txt[1] = "Je recherche notre position";
							Txt[2] = "Je recherche l'aéroport le plus proche";
							var methode_to_c='lost';
							
							sim_resquest.sim_state(url2,methode_to_c);
							/**/
							setTimeout(
								function()
									{
										SARAH.speak('extraction des informations');
										
										console.log("start");
										console.log(data_req_state);
										//dump(data_req_state);
										console.log("end");
									}
								,7000);
							/**/
							
						break;
						
						default:
       						SARAH.speak('Monsieur je suis désolé mais je ne comprend pas');	

					}





				
			}
		
		// Config is available everywhere
		// var version = Config.modules.copilote_fsx_p3d.version;
		
		// SARAH is available everywhere
		// SARAH.speak('Bonjour !');
		
		// The function next() MUST be called ONCE when job is done, 
		// with relevant data to call the next plugin by rule engine.
		
		if (Txt!=undefined)
			{
				Choix = Math.floor(Math.random() * Txt.length);
				SARAH.speak(Txt[Choix], function(){
					//console.log('After the speech');  
  				});
				//next();
			};
		if (process!=undefined)
			{
				var child = exec(process,function (error, stdout, stderr) {
								console.log(process);
						});
			};

		next({ });
	}

// ------------------------------------------
//  PLUGINS FUNCITONS
// ------------------------------------------

exports.init = function(){
	// Initialize resources of this plugin
	// All declared variables are scoped to this plugin 
	
	info('Plugin copilote_fsx_p3d is initializing ...');
}

exports.dispose = function(){
	// Dispose all resources no longer need 
	// a new instance of this plugin will be called
	// This function do not stand async code !
	
	info('Plugin copilote_fsx_p3d is disposed ...');
}

exports.ajax = function(req, res, next){
	// Called before rendering portlet when clicking on 
	// <a href="/plugin/copilote_fsx_p3d" data-action="ajax">click me</a>  
	// Because portlet CAN'T do asynchronous code
	console.log(req.query.prg);
	next();
}

exports.speak = function(tts, async){
	// Hook called for each SARAH.speak()
	// to perform change on string
	// return false to prevent speaking
	// info('Speaking : %s', tts, async);
	return tts;
}

exports.standBy = function(motion, device){
	// Hook called for each motion in a given device
	// to start/stop action when there is no moves
	//info('Motion on %s: %s', device, motion);
}
